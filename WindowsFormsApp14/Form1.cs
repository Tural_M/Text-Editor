﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp14
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
            fontDialog1.ShowDialog();
            richTextBox1.Font = fontDialog1.Font;
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (richTextBox1.TextLength != 0)
            {
                if (MessageBox.Show("Do you want to save changes ? ", "Text Editor", MessageBoxButtons.OKCancel, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    saveToolStripMenuItem_Click(sender, e);
                }
            }
            richTextBox1.Clear();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Text Document (*txt)|* .txt";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Text Document (*txt)|* .txt";
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
            }
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printDialog1.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            toolStripTextBox1.Clear();

        }

       

        private void toolStripTextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                int len = richTextBox1.TextLength;
                int index = 0;
                int lastIndex = richTextBox1.Text.LastIndexOf(toolStripTextBox1.Text);

                while (index < lastIndex)
                {
                   richTextBox1.Find(toolStripTextBox1.Text, index, len, RichTextBoxFinds.None);
                    richTextBox1.SelectionBackColor = Color.Yellow;
                    index = richTextBox1.Text.IndexOf(toolStripTextBox1.Text, index) + 1;
                }
            }
            else if(e.KeyChar == (char)Keys.Back)
            {
                int len = richTextBox1.TextLength;
                int index = 0;
                int lastIndex = richTextBox1.Text.LastIndexOf(toolStripTextBox1.Text);

                while (index < lastIndex)
                {
                    richTextBox1.Find(toolStripTextBox1.Text, index, len, RichTextBoxFinds.None);
                    richTextBox1.SelectionBackColor = Color.White;
                    index = richTextBox1.Text.IndexOf(toolStripTextBox1.Text, index) + 1;
                }
            }
        }
        

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Undo();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MaximizeBox = false;
            cutToolStripMenuItem.Enabled = false;
            copyToolStripMenuItem.Enabled = false;
            pasteToolStripMenuItem.Enabled = false;
            menuStrip1.BackColor = Color.Gray;
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
           
           
           if(string.IsNullOrWhiteSpace(richTextBox1.Text))
            {
                cutToolStripMenuItem.Enabled = false;
                copyToolStripMenuItem.Enabled = false;
                pasteToolStripMenuItem.Enabled = false;
            }
           else
            {
                cutToolStripMenuItem.Enabled = true;
                copyToolStripMenuItem.Enabled = true;
                pasteToolStripMenuItem.Enabled = true;
            }
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();

        }

        private void customizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fontDialog1.ShowColor = true;
            fontDialog1.ShowDialog();
            richTextBox1.Font = fontDialog1.Font;
            richTextBox1.SelectionColor = fontDialog1.Color;
            
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Redo();
        }
    }
}
